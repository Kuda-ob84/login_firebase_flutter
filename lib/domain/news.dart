import 'package:flutter/material.dart';

class News {
  final String id;
  
  String title;
  String author;
  String description;
  DateTime dateTime;

  News({this.id, this.title, this.author, this.description, this.dateTime});

  News.fromJson(Map<String, dynamic> data, {@required this.id}) {
    title = data['title'];
    author = data['author'];
    description = data['description'];
    dateTime = data['date'];
  }

  Map<String, dynamic> toMap() {
    return {
      "title": title,
      "description": description,
      "author": author,
      "date": dateTime
    };
  }
}
