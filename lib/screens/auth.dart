import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:login_firebase_flutter/domain/customer.dart';
import 'package:login_firebase_flutter/services/auth.dart';

class AuthorizationScreen extends StatefulWidget {
  @override
  _AuthorizationScreenState createState() => _AuthorizationScreenState();
}

class _AuthorizationScreenState extends State<AuthorizationScreen> {
  TextEditingController _emailController = TextEditingController();
  TextEditingController _passwordController = TextEditingController();
  TextEditingController _phoneController = TextEditingController();
  TextEditingController _codeController = TextEditingController();
  String _email;
  String _password;
  String _phone;
  String _code;
  bool isLogin = true;

  AuthService _authService = AuthService();
  FirebaseAuth firebaseAuth = FirebaseAuth.instance;

  @override
  Widget build(BuildContext context) {
    _phoneController.text = "+7";
    return MaterialApp(
        home: DefaultTabController(
            length: 2,
            child: Scaffold(
              appBar: AppBar(
                title: Text("Authorization screen"),
                bottom: TabBar(
                  tabs: [Tab(text: "EMAIL"), Tab(text: "PHONE")],
                ),
              ),
              body: _buildScaffoldBody(context),
            )));
  }

  Widget _buildScaffoldBody(BuildContext context) {
    return TabBarView(children: [
      Container(
          child: Padding(
        padding: EdgeInsets.symmetric(horizontal: 18, vertical: 25),
        child: Column(children: [
          _customTextField("EMAIL", _emailController, Icon(Icons.email), false),
          _customTextField(
              "PASSWORD", _passwordController, Icon(Icons.lock), true),
          Padding(
            padding: const EdgeInsets.only(top: 15),
            child: Container(
                width: double.infinity,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(16),
                  color: Colors.blue,
                ),
                child: isLogin
                    ? _buildButton("LOGIN", () {
                        _loginEvent();
                      })
                    : _buildButton("REGISTER", () {
                        _registerEvent();
                      })),
          ),
          Padding(
              padding: const EdgeInsets.only(top: 18.0),
              child: isLogin
                  ? _buildInkWell("Not registered yet? Register!")
                  : _buildInkWell("Already registered? Login!"))
        ]),
      )),
      Container(
          child: Padding(
        padding: EdgeInsets.symmetric(horizontal: 18, vertical: 25),
        child: Column(children: [
          _customTextField("PHONE NUMBER", _phoneController,
              Icon(Icons.phone_callback), false),
          // _customTextField(
          //     "PASSWORD", _passwordController, Icon(Icons.lock), true),
          Padding(
              padding: const EdgeInsets.only(top: 15),
              child: Container(
                  width: double.infinity,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(16),
                    color: Colors.blue,
                  ),
                  child: _buildButton("LOGIN", () async {
                    _phone = _phoneController.text.trim();
                    await _authService.signUpWithPhone(
                        _phone, context, _codeController);
                  }))),
        ]),
      ))
    ]);
  }

  Widget _buildInkWell(String text) {
    return InkWell(
        child: Text(
          text,
          style: TextStyle(fontSize: 16),
        ),
        onTap: () {
          setState(() {
            isLogin = !isLogin;
            _emailController.clear();
            _passwordController.clear();
          });
        });
  }

  Widget _buildButton(String text, void function()) {
    return FlatButton(
        child: Text(text, style: TextStyle(color: Colors.white, fontSize: 16)),
        onPressed: () {
          function();
        });
  }

  Widget _customTextField(
      String hintText,
      TextEditingController textEditingController,
      Icon icon,
      bool obscureText) {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: 10),
      child: TextField(
          obscureText: obscureText,
          controller: textEditingController,
          decoration: InputDecoration(
              prefixIcon: icon,
              hintText: hintText,
              border: OutlineInputBorder())),
    );
  }

  void _loginEvent() async {
    print("Login");
    _email = _emailController.text;
    _password = _passwordController.text;

    if (_email.isEmpty || _password.isEmpty) {
      return;
    }

    Customer customer = await _authService.signInWithEmailAndPassword(
        _email.trim(), _password.trim());

    if (customer == null) {
      Fluttertoast.showToast(
          msg: "User does not exist",
          gravity: ToastGravity.CENTER,
          backgroundColor: Colors.red,
          textColor: Colors.white);
    } else {
      _emailController.clear();
      _passwordController.clear();
    }
  }

  void _registerEvent() async {
    print("Register");
    _email = _emailController.text;
    _password = _passwordController.text;

    if (_email.isEmpty || _password.isEmpty) return;

    Customer customer = await _authService.signUpWithEmailAndPassword(
        _email.trim(), _password.trim());

    if (customer == null) {
      Fluttertoast.showToast(
          msg: "Check your email/password",
          gravity: ToastGravity.CENTER,
          backgroundColor: Colors.red,
          textColor: Colors.white);
    } else {
      _emailController.clear();
      _passwordController.clear();
    }
  }
}
