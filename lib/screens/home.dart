import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:login_firebase_flutter/domain/news.dart';
import 'package:login_firebase_flutter/services/auth.dart';

class HomeScreen extends StatelessWidget {
  AuthService _authService = new AuthService();

  @override
  Widget build(BuildContext context) {
    var customer = _authService.firebaseAuth.currentUser;
    return Scaffold(
      appBar: AppBar(title: Text("Home screen")),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 18, vertical: 15),
        child: Container(
            child: Column(
          children: [
            Text(
                "Logged as ${customer.phoneNumber == null ? customer.email : customer.phoneNumber}"),
            _buildButton("Log Out", () => _authService.logOut()),
            _buildButton("Add news", () => _addData())
          ],
        )),
      ),
    );
  }

  void _addData() {
    CollectionReference collectionReference =
        FirebaseFirestore.instance.collection("data");
    var news = new News(
        id: "1",
        title: "Test title",
        author: "Test author",
        description: "Test description",
        dateTime: DateTime.now());
    var demoData = news.toMap();
    collectionReference.add(demoData);
  }

  Widget _buildButton(String text, dynamic function()) {
    return Container(
      margin: const EdgeInsets.all(20),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(18), color: Colors.blue),
      width: double.infinity,
      child: FlatButton(
        child: Text(
          text,
          style: TextStyle(color: Colors.white, fontSize: 16),
        ),
        onPressed: () {
          function();
        },
      ),
    );
  }
}
