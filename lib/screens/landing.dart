import 'package:flutter/material.dart';
import 'package:login_firebase_flutter/domain/customer.dart';
import 'package:login_firebase_flutter/screens/auth.dart';
import 'package:login_firebase_flutter/screens/home.dart';
import 'package:provider/provider.dart';

class LandingPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final Customer customer = Provider.of<Customer>(context);
    final bool isLoggedIn = customer != null;
    return isLoggedIn ? HomeScreen() : AuthorizationScreen();
  }
}
