import 'package:flutter/material.dart';
import 'package:login_firebase_flutter/domain/customer.dart';
import 'package:login_firebase_flutter/screens/landing.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:login_firebase_flutter/services/auth.dart';
import 'package:provider/provider.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return StreamProvider<Customer>.value(
      value: AuthService().currentCustomer,
      child: MaterialApp(
        theme: ThemeData(primaryColor: Colors.blue),
        home: LandingPage(),
      ),
    );
  }
}
