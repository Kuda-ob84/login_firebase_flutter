import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:login_firebase_flutter/domain/customer.dart';

class AuthService {
  final FirebaseAuth firebaseAuth = FirebaseAuth.instance;

  Future<Customer> signInWithEmailAndPassword(
      String email, String password) async {
    try {
      UserCredential userCredential = await firebaseAuth
          .signInWithEmailAndPassword(email: email, password: password);
      User user = userCredential.user;
      return Customer.fromFirebase(user);
    } catch (e) {
      print(e);
      return null;
    }
  }

  Future<Customer> signUpWithEmailAndPassword(
      String email, String password) async {
    try {
      UserCredential userCredential = await firebaseAuth
          .createUserWithEmailAndPassword(email: email, password: password);
      User user = userCredential.user;
      return Customer.fromFirebase(user);
    } catch (e) {
      print(e);
      return null;
    }
  }

  // ignore: missing_return
  Future<Customer> signUpWithPhone(String phone, BuildContext context,
      TextEditingController _codeController) async {
    try {
      firebaseAuth.verifyPhoneNumber(
          phoneNumber: phone,
          timeout: Duration(seconds: 60),
          verificationCompleted: (AuthCredential credential) async {
            UserCredential userCredential =
                await firebaseAuth.signInWithCredential(credential);
            User user = userCredential.user;
            return Customer.fromFirebase(user);
          },
          verificationFailed: (FirebaseAuthException exception) {
            print(exception);
          },
          codeSent: (String verificationId, [int forceResendingToken]) {
            showDialog(
                context: context,
                barrierDismissible: false,
                builder: (context) {
                  return AlertDialog(
                    title: Text("Give the code?"),
                    content: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        TextField(
                          controller: _codeController,
                        ),
                      ],
                    ),
                    actions: <Widget>[
                      FlatButton(
                        child: Text("Confirm"),
                        textColor: Colors.white,
                        color: Colors.blue,
                        onPressed: () async {
                          final code = _codeController.text.trim();
                          AuthCredential credential =
                              PhoneAuthProvider.credential(
                                  verificationId: verificationId,
                                  smsCode: code);
                          UserCredential userCredential = await firebaseAuth
                              .signInWithCredential(credential);
                          User user = userCredential.user;
                          Navigator.pop(context);

                          return Customer.fromFirebase(user);
                        },
                      )
                    ],
                  );
                });
          },
          codeAutoRetrievalTimeout: (String verificationId) {});
    } catch (e) {
      print(e);
      return null;
    }
  }

  Future logOut() async {
    await firebaseAuth.signOut();
  }

  Stream<Customer> get currentCustomer {
    return firebaseAuth
        .authStateChanges()
        .map((User user) => user != null ? Customer.fromFirebase(user) : null);
  }
}
